import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../../../data/helper/painter.dart';
import '../controllers/face_detection_controller.dart';

class FaceDetectionView extends GetView<FaceDetectionController> {
  const FaceDetectionView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetBuilder<FaceDetectionController>(
      init: FaceDetectionController(),
      builder: (controller) {
        return Scaffold(
          appBar: AppBar(
            title: const Text("Face detection"),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: controller.getImage,
            child: const Icon(Icons.camera),
          ),
          body: Center(
            child: Column(
              children: [
                FittedBox(
                  child: SizedBox(
                    height: controller.image == null
                        ? Get.height.h / 2
                        : controller.image?.height.toDouble(),
                    width: controller.image == null
                        ? Get.width.w
                        : controller.image?.width.toDouble(),
                    child: CustomPaint(
                      painter: Painter(controller.rects, controller.image),
                    ),
                  ),
                ),
                const SizedBox(height: 30),
                if (controller.image != null)
                  Text(
                    controller.isSmiling ? "Smiling 😀" : "Not Smiling 😕",
                    style: const TextStyle(fontSize: 20),
                  ),
              ],
            ),
          ),
        );
      },
    );
  }
}
