import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_ml_kit/google_ml_kit.dart';
import 'dart:ui' as ui;

import 'package:image_picker/image_picker.dart';

class FaceDetectionController extends GetxController {
  ui.Image? image;
  List<Rect> rects = [];
  bool isSmiling = false;

  Future getImage() async {
    debugPrint('getImage');

    XFile? imagePickedFile =
        await ImagePicker().pickImage(source: ImageSource.gallery);
    final inputImage = InputImage.fromFilePath(imagePickedFile!.path);

    final faceDetector = GoogleMlKit.vision.faceDetector(
      FaceDetectorOptions(
        enableLandmarks: true,
        enableClassification: true,
      ),
    );

    final List<Face> faces = await faceDetector.processImage(inputImage);
    debugPrint('faces: ${faces.first.trackingId}');
    update();
    rects.clear();
    for (Face face in faces) {
      debugPrint('for faces ');
      debugPrint('${face.smilingProbability}');

      final double? rotX =
          face.headEulerAngleX; // Head is tilted up and down rotX degrees
      final double? rotY =
          face.headEulerAngleY; // Head is rotated to the right rotY degrees
      final double? rotZ = face.headEulerAngleZ;

      debugPrint('rotX : $rotX');
      debugPrint('rotY : $rotY');
      debugPrint('rotZ : $rotZ');

      if (face.smilingProbability != null) {
        if (face.smilingProbability! >= 0.6) {
          isSmiling = true;
        } else {
          isSmiling = false;
        }
      }

      final FaceLandmark? leftEar = face.landmarks[FaceLandmarkType.leftEar];
      if (leftEar != null) {
        final Point<int> leftEarPos = leftEar.position;
        debugPrint('leftEarPos : $leftEarPos');
      }

      // If classification was enabled with FaceDetectorOptions:
      if (face.smilingProbability != null) {
        final double? smileProb = face.smilingProbability;
        debugPrint('smileProb : $smileProb');
      }

      // If face tracking was enabled with FaceDetectorOptions:
      if (face.trackingId != null) {
        final int? id = face.trackingId;
        debugPrint('trackingId : $id');
      }
      update();
      rects.add(face.boundingBox);
    }

    var bytesFromImageFile = await imagePickedFile.readAsBytes();
    decodeImageFromList(bytesFromImageFile).then((img) {
      image = img;
      update();
    });
  }
}
