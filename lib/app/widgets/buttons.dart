import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'texts.dart';

class Buttons {
  static Widget defaultButton({
    required VoidCallback handler,
    required Widget widget,
    Color? fillColor,
    bool filled = true,
    EdgeInsets padding = const EdgeInsets.only(top: 10),
    double borderRadius = 5,
  }) {
    return Padding(
      padding: padding,
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: filled
              ? null
              : MaterialStateProperty.all<Color?>(Colors.transparent),
          elevation: filled ? null : MaterialStateProperty.all<double?>(0),
          shape: filled
              ? MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(borderRadius),
                  ),
                )
              : null,
        ),
        onPressed: handler,
        child: widget,
      ),
    );
  }

  static Widget textButton({
    required VoidCallback handler,
    required Widget widget,
  }) {
    return Padding(
      padding: EdgeInsets.only(top: 10.h),
      child: TextButton(
        onPressed: handler,
        child: widget,
      ),
    );
  }

  static Widget buttonCard({
    required String title,
    String? subtitle,
    required IconData iconData,
    required VoidCallback handler,
    bool isTrailing = false,
  }) {
    return Column(
      children: [
        InkWell(
          onTap: handler,
          child: Container(
            margin: EdgeInsets.all(10.sp),
            padding: EdgeInsets.all(10.sp),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Icon(
                      iconData,
                      color: Get.theme.primaryColor,
                    ),
                    SizedBox(
                      width: 20.w,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Texts.body1(title),
                        SizedBox(
                          height: 5.h,
                        ),
                        subtitle != null
                            ? Texts.body2(subtitle)
                            : const SizedBox(),
                      ],
                    ),
                  ],
                ),
                isTrailing
                    ? const Icon(
                        Icons.chevron_right,
                      )
                    : const SizedBox(),
              ],
            ),
          ),
        ),
        Divider(
          height: 0.h,
          thickness: .5,
        ),
      ],
    );
  }

  static Widget buttonCard2({
    required String title,
    String? subtitle,
    required IconData iconData,
    required VoidCallback handler,
    bool isTrailing = false,
  }) {
    return Column(
      children: [
        InkWell(
          onTap: handler,
          child: Container(
              margin: EdgeInsets.only(left: 10.w, right: 10.w),
              padding: EdgeInsets.only(top: 10.h, left: 10.w, right: 10.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.all(10.sp),
                    decoration: BoxDecoration(
                      color: Get.theme.primaryColor.withAlpha(50),
                      borderRadius: BorderRadius.circular(15.r),
                    ),
                    child: Icon(
                      iconData,
                      color: Get.theme.primaryColor,
                      size: 35.sp,
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: 10.w),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Texts.body1(title),
                                  SizedBox(
                                    height: 5.h,
                                  ),
                                  subtitle != null
                                      ? Texts.body2(
                                          subtitle,
                                          color: Colors.grey,
                                        )
                                      : const SizedBox(),
                                ],
                              ),
                              isTrailing
                                  ? const Icon(
                                      Icons.chevron_right,
                                    )
                                  : const SizedBox(),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10.h,
                        ),
                        Divider(
                          height: 0.h,
                          thickness: .3.h,
                        ),
                      ],
                    ),
                  )
                ],
              )

              // Row(
              //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //   children: [
              //     Row(
              //       children: [
              //         Icon(
              //           iconData,
              //           color: Get.theme.primaryColor,
              //         ),
              //         SizedBox(
              //           width: 20.w,
              //         ),
              //         Column(
              //           crossAxisAlignment: CrossAxisAlignment.start,
              //           children: [
              //             Texts.body1(title),
              //             SizedBox(
              //               height: 5.h,
              //             ),
              //             subtitle != null
              //                 ? Texts.body2(
              //                     subtitle,
              //                     color: Colors.grey,
              //                   )
              //                 : const SizedBox(),
              //             Divider(
              //               height: 0.h,
              //               thickness: 1.h,
              //             ),
              //           ],
              //         ),
              //       ],
              //     ),
              //     const Icon(
              //       Icons.chevron_right,
              //     )
              //   ],
              // ),
              ),
        ),
      ],
    );
  }
}
