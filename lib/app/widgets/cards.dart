import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import './texts.dart';

class Cards {
  static Widget menuCard(
    IconCardItem iconCard,
    VoidCallback functionHanler, {
    bool useCard = true,
    bool isSuspend = false,
    VoidCallback? suspendHanler,
  }) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        splashColor: Get.theme.primaryColor.withAlpha(100),
        onTap: functionHanler,
        // onTap: isSuspend ? suspendHanler : functionHanler,
        child: FittedBox(
          fit: BoxFit.scaleDown,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                decoration: !useCard
                    ? null
                    : BoxDecoration(
                        color: Get.theme.colorScheme.background,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10)),
                        boxShadow: [
                          BoxShadow(
                            color: Get.theme.shadowColor,
                            offset: const Offset(0.0, 1.0), //(x,y)
                            blurRadius: 1.0,
                          ),
                        ],
                      ),
                foregroundDecoration: !isSuspend
                    ? null
                    : const BoxDecoration(
                        color: Colors.blueGrey,
                        backgroundBlendMode: BlendMode.saturation,
                      ),
                child: Padding(
                    padding:
                        useCard ? const EdgeInsets.all(10.0) : EdgeInsets.zero,
                    child: Icon(
                      iconCard.iconData,
                      color: Get.theme.primaryColor,
                      size: 30,
                    )),
              ),
              useCard
                  ? const SizedBox(
                      height: 5,
                    )
                  : const SizedBox(
                      height: 3,
                    ),
              Texts.overline(
                iconCard.label,
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }

  static Widget bottomSheetMenu(
    IconCardItem iconCard,
    VoidCallback functionHandler, {
    bool useCard = true,
    bool isSuspend = false,
    VoidCallback? suspendHandler,
  }) {
    return Column(
      children: [
        Container(
          decoration: !useCard
              ? null
              : BoxDecoration(
                  shape: BoxShape.circle,
                  color: Get.theme.colorScheme.background,
                  boxShadow: [
                      BoxShadow(
                        color: Get.theme.shadowColor,
                        offset: const Offset(0.0, 1.0), //(x,y)
                        blurRadius: 1.0,
                      ),
                    ]),
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              borderRadius: BorderRadius.circular(100.r),
              onTap: isSuspend ? suspendHandler : functionHandler,
              child: Container(
                  padding: EdgeInsets.all(10.sp),
                  decoration: const BoxDecoration(
                    color: Colors.transparent,
                  ),
                  child:
                      // iconCard.isIcon
                      //     ?
                      Icon(
                    iconCard.iconData,
                    color: Get.theme.primaryColor,
                    size: 50.sp,
                  )
                  // : Image(
                  //     width: Get.width / 10.w,
                  //     height: Get.width / 10.h,
                  //     image: AssetImage(iconCard.imageAsset),
                  //   ),
                  ),
            ),
          ),
        ),
        useCard
            ? const SizedBox(
                height: 15,
              )
            : const SizedBox(
                height: 3,
              ),
        Texts.overline(
          iconCard.label,
          textAlign: TextAlign.center,
        ),
      ],
    );
  }
}

class IconCardItem {
  final String? imageAsset;
  final String label;
  final IconData? iconData;
  final bool isIcon;

  IconCardItem({
    this.imageAsset,
    required this.label,
    this.iconData,
    this.isIcon = false,
  });
}
