import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:image_picker/image_picker.dart';

import './cards.dart';
import './texts.dart';

class BottomSheets {
  static Widget cameraOption() {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.only(
          top: 10.h,
          bottom: 20.h,
          left: 20.w,
          right: 20.w,
        ),
        decoration: BoxDecoration(
          color: Get.theme.colorScheme.background,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15.r), topRight: Radius.circular(15.r)),
          boxShadow: [
            BoxShadow(
              blurRadius: 30,
              offset: const Offset(0, 5),
              color: Get.theme.shadowColor,
            )
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: Padding(
                padding: EdgeInsets.all(8.0.sp),
                child: Texts.headline6("Pilih Gambar"),
              ),
            ),
            const Divider(),
            Padding(
              padding: EdgeInsets.only(top: 20.h, bottom: 10.h),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Cards.bottomSheetMenu(
                    IconCardItem(
                      iconData: Icons.camera_alt,
                      label: "Kamera",
                    ),
                    () {
                      Get.back(result: ImageSource.camera);
                    },
                  ),
                  Cards.bottomSheetMenu(
                    IconCardItem(
                      iconData: Icons.photo,
                      label: "Galeri",
                    ),
                    () {
                      Get.back(result: ImageSource.gallery);
                    },
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
