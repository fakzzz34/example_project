import 'package:image_picker/image_picker.dart';

enum ImageSourceType { xfile, url }

enum ImagePickType { single, multiple }

class ImageUploadModel {
  final String id;
  final String title;
  final String? text;
  XFile? file;
  List<XFile>? listFile;
  String? imageUrl;
  ImageSourceType sourceType;
  ImagePickType pickType;
  bool valid = true;

  reset() {
    valid = true;
    file = null;
  }

  ImageUploadModel({
    required this.id,
    required this.title,
    this.text,
    this.file,
    this.imageUrl,
    this.sourceType = ImageSourceType.xfile,
    this.pickType = ImagePickType.single,
  });

  bool isNotEmpty() {
    return imageUrl?.isNotEmpty ?? false || file != null;
  }

  updateImage(XFile? pickImage) {
    valid = true;
    sourceType = ImageSourceType.xfile;
    file = pickImage;
  }

  updateListImage(List<XFile>? pickImage) {
    valid = true;
    sourceType = ImageSourceType.xfile;
    listFile = pickImage;
  }

  bool checkValid() {
    if (pickType == ImagePickType.single) {
      valid = imageUrl?.isNotEmpty ?? false || file != null;
      return valid;
    } else {
      valid = imageUrl?.isNotEmpty ?? false || listFile != null;
      return valid;
    }
  }

  static ImageUploadModel emptyController() {
    return ImageUploadModel(id: "", title: "");
  }
}
